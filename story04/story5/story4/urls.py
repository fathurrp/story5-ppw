from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('about/', views.about, name='about'),
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),

]