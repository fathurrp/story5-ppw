from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('about/', views.about, name='about'),
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    
]